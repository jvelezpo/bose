import { Store, Dispatch } from 'redux';
import * as ReduxPromise from 'redux-promise';

import { REHYDRATE } from 'redux-persist/constants';
import * as createActionBuffer from 'redux-action-buffer';

const middlewares = [createActionBuffer(REHYDRATE), ReduxPromise];

export default middlewares;
