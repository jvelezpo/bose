import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Auth from './auth'
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import Store from './store';

const StoreInstance = Store();

const auth = new Auth();

ReactDOM.render(
  <Provider store={StoreInstance.store}>
    <App auth={auth} />
  </Provider>,
  document.getElementById('root'));

registerServiceWorker();
