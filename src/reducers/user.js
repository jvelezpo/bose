import {
  LOGIN_USER,
  LOGOUT_USER
} from '../constants';
import Store from '../store';

const getInitialState = () => {
  return {};
}

export default(state = getInitialState(), payload) => {
  switch(payload.type) {
    case LOGIN_USER:
      return payload.auth
    case LOGOUT_USER:
      const StoreInstance = Store();
      StoreInstance.persistor.purge();
      return getInitialState();
    default:
      return state;
  }
};
