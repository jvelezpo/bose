import React, { Component } from 'react';
import { Grid, Navbar, Nav, NavItem } from 'react-bootstrap';
import {
  Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import history from './history';
import Callback from './components/callback';
import Profile from './components/profile';
import { login, logout } from './actions/auth';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './App.css';

const Home = () => (
  <p>
    Home
  </p>
);

const Login = () => (
  <p>
    Login
  </p>
);

const NoMatch = ({ location }) => (
  <div>
    <h3>
      No match for <code>{location.pathname}</code>
    </h3>
  </div>
);

const Signup = () => (
  <p>
    Signup
  </p>
)

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route
    {...rest}
    render = {props => 
      isAuthenticated ? (
        <Component {...rest} {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/404",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

class App extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  goTo(route) {
    history.replace(`/${route}`)
  }

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout(this.props.logout);
  }

  handleAuthentication = (nextState, replace) => {
    console.log('next', nextState);
    if (/access_token|id_token|error/.test(nextState.location.hash)) {
      console.log('true')
      this.props.auth.handleAuthentication(this.props.login);
    }
  }

  render() {
    console.log('props', this.props);
    const { isAuthenticated } = this.props.auth;
    console.log('isAuthenticated', isAuthenticated())

    return (
      <Router history={history} component={App}>
        <div>
          <Navbar inverse>
            <Navbar.Header>
              <Navbar.Brand>
                <a onClick={() => this.goTo('')}>Bose</a>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              { !isAuthenticated() && (
                <Nav pullRight>
                  <NavItem eventKey={1} onClick={this.login}>
                    Login
                  </NavItem>
                  <NavItem eventKey={2} href="/signup">
                    SignUp
                  </NavItem>
                </Nav>
              )}
              { isAuthenticated() && (
                <Nav pullRight>
                  <NavItem eventKey={1} onClick={() => this.goTo('profile')}>
                    Profile
                  </NavItem>
                  <NavItem eventKey={2} onClick={this.logout}>
                    Logout
                  </NavItem>
                </Nav>
              )}
            </Navbar.Collapse>
          </Navbar>
    
          <Grid>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/login" component={Login} />
              <Route path='/signup' component={Signup} />
              {/* <Route path="/profile" render={(props) => <Profile auth={this.props.auth} {...props} />} /> */}
              <PrivateRoute path="/profile" component={Profile} isAuthenticated={isAuthenticated()} auth={this.props.auth} />
              <Route path="/callback" render={(props) => {
                this.handleAuthentication(props);
                return <Callback {...props} /> 
              }}/>
              <Route component={NoMatch} />
            </Switch>
          </Grid>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state, prop) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    login: bindActionCreators(login, dispatch),
    logout: bindActionCreators(logout, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);