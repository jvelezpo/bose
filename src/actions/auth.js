import {
  LOGIN_USER,
  LOGOUT_USER
} from '../constants';

export const login = (auth) => {
  console.log(LOGIN_USER, auth);
  return {
    type: LOGIN_USER,
    auth
  }
}

export const logout = (auth) => {
  console.log(LOGOUT_USER, auth);
  return {
    type: LOGOUT_USER
  }
}
